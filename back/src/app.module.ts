import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GameModule } from './game/game.module';
import { MongooseModule } from '@nestjs/mongoose';
import { NuggetsModule } from './nuggets/nuggets.module';


@Module({
  imports: [GameModule, MongooseModule.forRoot('mongodb://goli:golipass@localhost:27017/burger-quiz'), NuggetsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
