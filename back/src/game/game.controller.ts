import { Controller, Get, Post, Body, Patch } from '@nestjs/common';
import { GameService } from './game.service'

@Controller('game')
export class GameController {

  constructor(private gameService: GameService){}

  @Patch('point')
  point(@Body('team') team: string, @Body('type') type: string){
    this.gameService.changeScore(team, type);
    return this.gameService.score;
  }

  @Get('point')
  getPoint(): any {
    return this.gameService.score;
  }

  @Post('commonscreen')
  commonScreen(){
    this.gameService.changeCommonScreen();
    return this.gameService.gameParameters;
  }

  @Patch('current-team')
  currentTeam(@Body('team') team: string){
    this.gameService.changeCurrentTeam(team);
    return this.gameService.gameParameters;
  }
  
  @Patch('mode')
  mode(@Body('mode') mode:string,
  @Body('question') question: string,
  @Body('answer') answer: string,
  @Body('selections') selections: string[]){
    this.gameService.changeMode(mode, question, answer, selections);
    return {mode,question,answer,selections};
  }

  @Patch('select')
  select(@Body('selected') selected: number){
    this.gameService.changeSelection(selected);
  }

  @Patch('display')
  display(@Body('display') display: number){
    this.gameService.changeDisplay(display);
  }

  @Post('validanswer')
  validAnswer(){
    this.gameService.validAnswer();
  }

  @Patch('jingle')
  jingle(@Body('jingle') jingle: string){
    this.gameService.launchJingle(jingle);
    return {jingle};
  }

  @Get('parameters')
  getParameters(): any {
    return this.gameService.gameParameters;
  }

  @Post('reset')
  reset(){
    this.gameService.resetGame();
  }
}
