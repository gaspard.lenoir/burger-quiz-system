import * as mongoose from 'mongoose';

export interface NuggetsModel {
  theme: string;
  questions: NuggetsQuestionModel[];
}

export interface NuggetsQuestionModel {
  title: string;
  answers: string[];
  good: string;
}

export const NuggetsSchema = new mongoose.Schema({
  theme: String,
  questions: [{
    title: String,
    answers: Array,
    good: String,
  }]
});
