import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScreenComponent } from './screen/screen.component'
import { ManagerModule } from './manager/manager.module'

const routes: Routes = [
  { path: 'screen', component: ScreenComponent },
  { path: 'screen/:type', component: ScreenComponent },
  { path: 'manager', loadChildren: () => import('./manager/manager.module').then(m => m.ManagerModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
