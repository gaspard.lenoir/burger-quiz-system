import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { localServer } from '../app.config';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScreenComponent } from './screen/screen.component';
import { ScoringComponent } from './scoring/scoring.component';

import { NgxsModule } from '@ngxs/store';

import { ScoreState } from './store/scoring.store';
import { GameState } from './store/game.store';

// websockets

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { CommonScreenModule } from './common-screen/common-screen.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

const socketConfig: SocketIoConfig = { url: localServer, options: {}};

@NgModule({
  declarations: [
    AppComponent,
    ScreenComponent,
    ScoringComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SocketIoModule.forRoot(socketConfig),
    NgxsModule.forRoot([GameState, ScoreState]),
    BrowserAnimationsModule,
    CommonScreenModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
