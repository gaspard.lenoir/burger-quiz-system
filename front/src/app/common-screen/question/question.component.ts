import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit, OnChanges {

  @Input() team: string;
  @Input() question: string;
  @Input() selections: string[];
  @Input() selected: number;
  @Input() display: number;
  @Input() validated: boolean;
  @Input() answer: number;

  formatedSelections: selection[] = [];

  private letters = ['A','B','C','D'];

  constructor() { }

  ngOnChanges(){
    this.formatedSelections = [];
    for(let i in this.selections){
      const selection: selection = {answer: this.selections[i], classes: [], type: this.letters[i]};
      this.formatedSelections.push(selection);
    }

    this.formatedSelections.forEach(s => {
      s.classes.splice(s.classes.indexOf('selected'), 1);
    });

    if(this.selected >= 0){
      this.formatedSelections[this.selected].classes.push('selected');
    }

    if(this.display >= 0){
      for(let i = 0; i <= this.display; i++){
        this.formatedSelections[i].classes.push('display');
      }
    }
    else {
      this.formatedSelections.forEach(s => {
        s.classes.splice(s.classes.indexOf('display'), 1);
      })
    }
    if(this.validated){
      this.formatedSelections[this.selected].classes.push("wrong");
      this.formatedSelections[this.answer -1].classes.push("good");
    }
  }

  ngOnInit() {
  }

}

interface selection {
  answer: string;
  classes: string[];
  type: string | number;
}