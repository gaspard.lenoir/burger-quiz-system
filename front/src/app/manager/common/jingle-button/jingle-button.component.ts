import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { localServer } from '../../../../app.config';

@Component({
  selector: 'app-jingle-button',
  templateUrl: './jingle-button.component.html',
  styleUrls: ['./jingle-button.component.scss']
})
export class JingleButtonComponent implements OnInit {

  @Input() jingle: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient) { }

  launchJingle(){
    this.http.patch(localServer + 'game/jingle', {"jingle": this.jingle}, this.httpOptions).subscribe();
  }

  ngOnInit() {
  }

}
