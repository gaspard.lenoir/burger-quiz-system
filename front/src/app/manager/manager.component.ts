import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss']
})
export class ManagerComponent implements OnInit {

  score: any;
  gameParameters: any;

  mode: string;

  constructor( private gameService: GameService, private route: ActivatedRoute, private router: Router ) { }

  changeMode(evt:string){
    console.log(evt);
    this.router.navigate(['/manager', evt]);
    this.mode = evt;
  }

  ngOnInit() {
    this.mode = this.route.snapshot.paramMap.get("mode");

    this.gameService.score$.subscribe(v => {
      this.score = v;
    });
    this.gameService.game$.subscribe(v => {
      this.gameParameters = v;
    });
  }

}
