import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagerRoutingModule } from './manager-routing.module';
import { ManagerComponent } from './manager.component';
import { ScorebarComponent } from './scorebar/scorebar.component';

import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { TopbarComponent } from './topbar/topbar.component';
import { ModesComponent } from './modes/modes.component';
import { NuggetsComponent } from './modes/nuggets/nuggets.component';
import { QuestionsComponent } from './modes/nuggets/questions/questions.component';
import { JingleButtonComponent } from './common/jingle-button/jingle-button.component';
import { MainComponent } from './modes/main/main.component';
import { MenusComponent } from './modes/menus/menus.component';
import { SelOuPoivreComponent } from './modes/sel-ou-poivre/sel-ou-poivre.component';

@NgModule({
  declarations: [ManagerComponent, ScorebarComponent, TopbarComponent, ModesComponent, NuggetsComponent, QuestionsComponent, JingleButtonComponent, MainComponent, MenusComponent, SelOuPoivreComponent],
  imports: [
    CommonModule,
    ManagerRoutingModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatSelectModule
  ]
})
export class ManagerModule { }
