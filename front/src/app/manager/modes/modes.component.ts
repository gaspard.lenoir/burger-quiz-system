import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modes',
  templateUrl: './modes.component.html',
  styleUrls: ['./modes.component.scss']
})
export class ModesComponent implements OnInit {

  @Input() mode: string;

  constructor() { }

  ngOnInit() {
  }

}
