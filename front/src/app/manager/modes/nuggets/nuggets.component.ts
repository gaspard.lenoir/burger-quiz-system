import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {localServer} from '../../../../app.config';

@Component({
  selector: 'app-ma-nuggets',
  templateUrl: './nuggets.component.html',
  styleUrls: ['./nuggets.component.scss']
})
export class NuggetsComponent implements OnInit {

  themes: any;
  theme: any;
  id: string;
  // question: any;
  nq: number = 0;
  tq: number;

  constructor(private http: HttpClient) { }

  selectTheme(id: string){
    console.log(id);
    this.id = id;
    this.http.get(localServer + 'nuggets/' + id).subscribe(val => {
      this.theme = val;
      this.tq = this.theme.questions.length;
      console.log(val);
    });
  }

  nextQuestion(){
    this.nq++;
  }
  
  ngOnInit() {
    this.http.get(localServer + 'nuggets').subscribe(val => {
      this.themes = val;
      console.log(val);
    });
  }

}
