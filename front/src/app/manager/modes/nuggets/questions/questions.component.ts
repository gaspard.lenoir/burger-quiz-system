import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { localServer } from '../../../../../app.config';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit, OnChanges {

  @Input() question: any;
  @Input() end: boolean;
  @Output() next= new EventEmitter<undefined>();

  answers: Answer[] = [];
  state: 'started' | 'stopped' | 'answer' | 'next' | 'end';
  displayed = 0;
  total: number;
  canSelect = false;
  selected: number;

  private letters = ['A', 'B', 'C', 'D'];

  httpResponse: Observable<any>;
  // serverUrl = localServer + 'game/point';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient) { }

  onStart(){
    this.state = 'started';
    const payload = {"mode": "nuggets", "question": this.question.title, "answer": this.question.good, "selections": this.question.answers};
    this.http.patch(localServer + 'game/mode', payload, this.httpOptions).subscribe();
  }

  nextAnswer(){
    if(this.displayed < this.total){
      this.answers[this.displayed].classes.splice(
        this.answers[this.displayed].classes.indexOf('disabled'), 1);
        this.http.patch(localServer + 'game/display', {"display": this.displayed}, this.httpOptions).subscribe();
        this.displayed++;
      if(this.displayed == this.total){
        this.state = 'answer';
        this.canSelect = true;
      }
    }
  }

  selectQuestion(type){
    if(this.canSelect){
      this.answers.forEach(a => {
        a.classes.splice(a.classes.indexOf('selected'), 1);
      })
      this.answers[Number(type)].classes.push('selected');
      this.selected = type;
      console.log(this.selected);
      this.http.patch(localServer + 'game/select', {"selected": this.selected}, this.httpOptions).subscribe();
    }
  }

  validAnswer(){
    this.canSelect = false;
    this.answers[this.selected].classes.push('bad');
    this.answers[this.question.good - 1].classes.push('good');
    this.http.post(localServer + 'game/validanswer', {}, this.httpOptions).subscribe();
    if(!this.end){
      this.state = 'next';
    } else {
      this.state = 'end';
    }
  }

  nextQuestion(){
    this.next.emit(undefined);
  }

  ngOnInit() {
    // this.state = 'stopped';
  }

  ngOnChanges() {
    console.log(this.question)
    this.answers = [];
    for(let i in this.question.answers){
      const a: Answer = { answer: this.question.answers[i], classes: ['disabled'], type: Number(i)} ;
      this.answers.push(a);
    }
    this.total = this.question.answers.length;
    this.reset();
  }

  private reset(){
    this.state = 'stopped';
    this.displayed = 0;
    this.canSelect = false;
    this.selected = undefined;
    console.log(this.end);
  }

}

interface Answer {
  answer: string;
  classes: string[];
  type: string | number;
}