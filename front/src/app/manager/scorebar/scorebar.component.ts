import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { localServer } from '../../../app.config';

@Component({
  selector: 'app-scorebar',
  templateUrl: './scorebar.component.html',
  styleUrls: ['./scorebar.component.scss']
})
export class ScorebarComponent implements OnInit {

  @Input() score: any;

  httpResponse: Observable<any>;
  serverUrl = localServer + 'game/point';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient) { }

  addPoint(team: string){
    const payload = {"team": team, "type": "add"};
    this.http.patch(localServer + 'game/point', payload, this.httpOptions).subscribe();
  }

  removePoint(team: string){
    const payload = {"team": team, "type": "remove"};
    this.http.patch(localServer + 'game/point', payload, this.httpOptions).subscribe();
  }

  changeCurrentTeam(team: string){
    const payload = {"team": team};
    this.http.patch(localServer + 'game/current-team', payload, this.httpOptions).subscribe();
  }

  ngOnInit() {
  }

}
