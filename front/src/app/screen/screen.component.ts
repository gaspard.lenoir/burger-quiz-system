import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { GameService } from "../services/game.service";

@Component({
  selector: "app-screen",
  templateUrl: "./screen.component.html",
  styleUrls: ["./screen.component.scss"]
})
export class ScreenComponent implements OnInit {
  type: string;

  score: any;
  game: any;

  constructor(private route: ActivatedRoute, public gameService: GameService) {}

  ngOnInit() {
    this.type = this.route.snapshot.paramMap.get("type");

    this.gameService.score$.subscribe((val:any ) => {
      this.score = val;
    })

    this.gameService.game$.subscribe((val:any ) => {
      this.game = val;
    })
  }
}
